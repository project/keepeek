<?php

namespace Drupal\keepeek;

/**
 * Defines an interface for a formatter that deals with Remote Media assets.
 */
interface KeepeekFormatterInterface
{

    /**
     * Tries to identify a default name for a given URL.
     *
     * @param string $url
     *   The source URL being used for this media entity.
     *
     * @return \Drupal\Core\StringTranslation\TranslatableMarkup
     *   The translated string to be used in the source plugin as default name.
     */
    public static function deriveMediaDefaultNameFromUrl($url);

}
