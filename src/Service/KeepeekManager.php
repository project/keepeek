<?php

namespace Drupal\keepeek\Service;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\MediaInterface;

/**
 * KeepeekManager
 */
class KeepeekManager
{
    /**
     * @var string
     */
    public const PERMALINK_TYPE_ORIGINAL = 'original';
    public const PERMALINK_TYPE_PREVIEW = 'preview';
    public const PERMALINK_TYPE_MEDIUM = 'medium';
    public const PERMALINK_TYPE_XLARGE = 'xlarge';

    /**
     * @var string
     */
    protected const TYPE_NORMAL = 'type-normal';
    protected const TYPE_RESIZE = 'type-resize';

    /**
     * Keepeek extension pattern from WHR format or Dynameek
     */
    const KEEPEEK_EXTENSION_PATTERN = '(jpg|jpeg|png|webp|heif)';
    /**
     * Drupal image style conversion extension pattern
     */
    const DRUPAL_IMAGE_STYLE_CONVERSION_EXTENSION_PATTERN = '(jpg|jpeg|jpe|png|gif|webp)';

    /**
     * This method is used to correct the Drupal URL path.
     *
     * It takes a Drupal URL path as a string, checks if it contains a URL, and if so, it decodes the URL,
     * removes the 'itok' query parameter if it exists, checks for double extensions, and returns the corrected URL.
     *
     * - If the URL contains the 'itok' query parameter, it is removed.
     * - If the URL contains two extensions (e.g., ".jpg.webp"), the second extension is removed to avoid a 404 error.
     *
     * @param string $path Original Drupal URL path.
     * @return string Asset URL.
     */
    public static function getCorrectUrl(string $url): string
    {
        $pathArray = explode('https/', $url);
        if (count($pathArray) > 1) {
            $url = urldecode($pathArray[1]);
            $url = sprintf('https://%s', $url);

            // Remove itok for external urls
            $itokPos = strpos($url, '?itok=');
            if ($itokPos !== false) {
                $url = substr($url, 0, $itokPos);
            }

            // Detect if the URL contains a double extension where the first extension originates from the Keepeek image,
            // and the second extension is added by Drupal through an "image style" effect.
            // This occurs, for instance, when Drupal applies a format conversion (e.g., to WebP) on thumbnails in the media library.
            // The double extension should have first an available Keepeek extension
            // and the second an available in Drupal image style conversion extension.
            if (preg_match('/\.' . self::KEEPEEK_EXTENSION_PATTERN . '\.' . self::DRUPAL_IMAGE_STYLE_CONVERSION_EXTENSION_PATTERN . '$/', $url)) {
                // If a double extension is detected, remove the second extension to avoid a 404 error.
                $url = preg_replace('/\.' . self::DRUPAL_IMAGE_STYLE_CONVERSION_EXTENSION_PATTERN . '$/', '', $url);
            }
        }
        return $url;
    }

    /**
     * Extracts data from a field item.
     *
     * @param FieldItemInterface $item The field item to extract data from.
     * @return array The extracted data.
     */
    public static function getDataFromFieldItem(FieldItemInterface $item): array
    {
        $media = $item->getParent()->getParent()->getValue();
        $fieldName = self::getJsonFieldName($media);

        $data = [];
        if ($item->getParent() instanceof FieldItemListInterface
            && $media instanceof MediaInterface
            && $media->hasField($fieldName)
        ) {
            $data = self::getDataFromMedia($media);
        }
        return $data;
    }

    /**
     * Extracts data from a media item.
     *
     * @param MediaInterface $media The media item to extract data from.
     * @return array The extracted data.
     */
    public static function getDataFromMedia(MediaInterface $media): array
    {
        $fieldName = self::getJsonFieldName($media);

        $data = [];
        if ($media->hasField($fieldName)) {
            foreach ($media->get($fieldName)->getValue() as $mediaFieldValue) {
                $data = json_decode($mediaFieldValue['value'], true) ?? [];
            }
        }
        return $data;
    }

    /**
     * Gets the permalink URL of a given type from the provided data.
     *
     * @param array $data The data to extract the permalink URL from.
     * @param string $type The type of permalink URL to get.
     * @return string The permalink URL.
     */
    public static function getPermalinkUrl(array $data, string $type = self::PERMALINK_TYPE_ORIGINAL): string
    {
        $url = '';
        if (isset($data['permalinks'])) {
            foreach ($data['permalinks'] as $permalink) {
                if (isset($permalink['title']) && $permalink['title'] === $type) {
                    $url = $permalink['url'];
                    break;
                }
            }
        }
        return $url;
    }

    /**
     * Checks if the provided data represents a video.
     *
     * @param array $data The data to check.
     * @return bool True if the data represents a video, false otherwise.
     */
    public static function isVideo(array $data): bool
    {
        return str_contains($data['mediaType'], 'video');
    }

    /**
     * Extracts the filename from a URI.
     *
     * @param string $uri The URI to extract the filename from.
     * @return string The extracted filename.
     */
    public static function getFilenameFromUri(string $uri): string
    {
        if (self::getUriType($uri) === self::TYPE_RESIZE) {
            $uriValues = UrlHelper::parse($uri);
            $uri = $uriValues['query']['src'] ?? $uri;
        }
        return basename($uri);
    }

    /**
     * Gets the name of the JSON field from a media item.
     *
     * @param MediaInterface $media The media item to get the JSON field name from.
     * @return string The name of the JSON field.
     */
    public static function getJsonFieldName(MediaInterface $media)
    {
        $fieldMap = $media->bundle->entity->getFieldMap();

        foreach ($fieldMap as $fieldName => $fieldLabel) {
            if ($fieldName == "json") {
                return $fieldLabel;
            }
        }
    }

    /**
     * Determines the type of a URI.
     *
     * @param string $uri The URI to determine the type of.
     * @return string The type of the URI.
     */
    public static function getUriType(string $uri): string
    {
        return str_contains($uri, '/resize') || str_contains($uri, '/crop') ? self::TYPE_RESIZE : self::TYPE_NORMAL;
    }
}
