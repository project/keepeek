<?php

namespace Drupal\keepeek\Form;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Defines a form for configuring the Keepeek module.
 */
class KeepeekSettingsForm extends ConfigFormBase
{
    /**
     * @var array  
     */
    protected array $formItems = [
    'api_endpoint' => ['type' => 'url', 'attributes' => ['placeholder' => 'https://'], 'description' => 'Keepeek media library URL (ie. <code>https://www.keepeek.com</code>)'],
    'keycloak_client_id' => ['type' => 'textfield', 'required' => false],
    'keycloak_idp' => ['type' => 'textfield', 'required' => false],
    'data_locale' => ['type' => 'textfield', 'required' => false, 'description' => 'Two chars format (ie. <code>FR</code>)'],
    'ui_locale' => ['type' => 'textfield', 'required' => false, 'description' => 'Two chars format (ie. <code>FR</code>)'],
    ];

    /**
     * {@inheritdoc}
     */
    public function getEditableConfigNames()
    {
        return ['keepeek.settings'];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'keepeek_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('keepeek.settings');

        $form['configuration'] = [
        '#type' => 'details',
        '#title' => $this->t('Keepeek configuration'),
        '#open' => true,
        ];

        foreach ($this->formItems as $itemName => $itemValues) {
            $form['configuration'][$itemName] = [
            '#type' => $itemValues['type'],
            '#title' => $this->t($itemName),
            '#default_value' => $config->get($itemName),
            '#required' => $itemValues['required'] ?? TRUE,
            '#description' => $itemValues['description'] ?? NULL,
            '#attributes' => $itemValues['attributes'] ?? [],
            ];
        }

        $jwt_key = $config->get('jwt_key');
        if (empty($jwt_key)) {
            $jwt_key = $this->generateJwtKey();
        }
        $form['endpoint'] = [
            '#type' => 'fieldset',
        ];
        $form['endpoint']['jwt_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('JWT key'),
            '#description' => $this->t('Use this key for communication with the Keepeek endpoints.'),
            '#default_value' => $jwt_key,
            '#required' => TRUE,
        ];
        $form['endpoint']['jwt_key_refresh'] = [
            '#type' => 'submit',
            '#name' => 'jwt_key_refresh',
            '#value' => $this->t('Refresh JWT key'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $settings = $this->config('keepeek.settings');

        foreach ($this->formItems as $itemName => $itemValues) {
            $settings->set($itemName, $form_state->getValue($itemName));
        }

        // Refresh JWT key if requested.
        $element = $form_state->getTriggeringElement();
        if ($element['#name'] === 'jwt_key_refresh') {
            $settings->set('jwt_key', $this->generateJwtKey());
        }
        else {
            $settings->set('jwt_key', $form_state->getValue('jwt_key'));
        }

        $settings->save();

        parent::submitForm($form, $form_state);
    }

    /**
     * Generate JWT key.
     *
     * @return string
     *   The generated JWT key.
     */
    protected function generateJwtKey() {
        return Crypt::hmacBase64(time(), Settings::getHashSalt());
    }

}
