<?php

namespace Drupal\keepeek\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media_library\Form\AddFormBase;
use Drupal\keepeek\Service\KeepeekManager;
use const Drupal\keepeek\Plugin\media\Source\KEEPEEK_MEDIA_TYPE;

/**
 * Form to create media entities using the Keepeek source plugin.
 */
class KeepeekMediaForm extends AddFormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return $this->getBaseFormId() . '_keepeek';
    }

    /**
     * {@inheritdoc}
     */
    protected function buildInputElement(array $form, FormStateInterface $form_state)
    {
        $media_type = $this->getMediaType($form_state);
        $media_source = $media_type->getSource()->getConfiguration();
        $keepeek_media_type = [];
        if (isset($media_source[KEEPEEK_MEDIA_TYPE])) {
            $keepeek_media_type = $media_source[KEEPEEK_MEDIA_TYPE];
        }

        $form['#attached']['library'][] = "keepeek/keepeek_library";

        // Add a container to group the input elements for styling purposes.
        $form['container'] = [
            '#type' => 'container',
        ];

        $form['container']['picker'] = [
            '#type' => 'container',
            '#attributes' => array('id' => 'kpk-keepicker-container', 'data-keepeek-media-type' => json_encode($keepeek_media_type)),
        ];

        $form['container']['url'] = [
            '#type' => 'hidden',
            '#title' => $this->t('URL'),
            '#required' => true,
            '#attributes' => array('class' => array('keepicker-url')),
        ];

        $form['container']['json'] = [
            '#type' => 'hidden',
            '#title' => $this->t('JSON'),
            '#required' => true,
            '#attributes' => array('class' => array('keepicker-json')),
        ];

        $form['container']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Add'),
            '#button_type' => 'primary',
            '#validate' => ['::validateUrl'],
            '#submit' => ['::addButtonSubmit'],
            '#ajax' => [
                'callback' => '::updateFormCallback',
                'event' => 'click',
                'wrapper' => 'media-library-wrapper',
                'url' => Url::fromRoute('media_library.ui'),
                'options' => [
                    'query' => $this->getMediaLibraryState($form_state)->all() + [
                            FormBuilderInterface::AJAX_FORM_REQUEST => true,
                        ],
                ],
            ],
        ];
        return $form;
    }

    /**
     * Submit handler for the add button.
     *
     * @param array $form
     *   The form render array.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state.
     */
    public function addButtonSubmit(array $form, FormStateInterface $form_state)
    {
        $this->processInputValues([$form_state->getValue('url')], $form, $form_state);
    }

    /**
     * Validates the Keepeek URL.
     *
     * @param array $form
     *   The complete form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current form state.
     */
    public function validateUrl(array &$form, FormStateInterface $form_state)
    {
        $url = $form_state->getValue('url');
        $media_type = $this->getMediaType($form_state);
        $media_storage = $this->entityTypeManager->getStorage('media');
        $source_field_name = $this->getSourceFieldName($media_type);
        $media = $this->createMediaFromValue($media_type, $media_storage, $source_field_name, $url);
        if ($media) {
            $violations = $media->validate();
            if ($violations->count() > 0) {
                /**
                 * @var \Symfony\Component\Validator\ConstraintViolation $violation
                 */
                foreach ($violations as $violation) {
                    $form_state->setErrorByName('url', $violation->getMessage());
                }
            }
        } else {
            $form_state->setErrorByName('url', $this->t('Invalid URL.'));
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function buildEntityFormElement(MediaInterface $media, array $form, FormStateInterface $form_state, $delta)
    {
        $element = parent::buildEntityFormElement($media, $form, $form_state, $delta);
        $element['preview']['thumbnail'] = [
            '#theme' => 'image',
            '#style_name' => 'media_library',
            '#uri' => $form_state->getValue('url'),
        ];

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    protected function createMediaFromValue(MediaTypeInterface $media_type, EntityStorageInterface $media_storage, $source_field_name, $source_field_value)
    {
        $media = parent::createMediaFromValue($media_type, $media_storage, $source_field_name, $source_field_value);

        $fieldName = KeepeekManager::getJsonFieldName($media);

        $media->set($fieldName, '{}');

        return $media;
    }

    /**
     * {@inheritdoc}
     */
    protected function processInputValues(array $source_field_values, array $form, FormStateInterface $form_state)
    {
        parent::processInputValues($source_field_values, $form, $form_state);

        foreach ($form_state->get('media') as $media) {
            $fieldName = KeepeekManager::getJsonFieldName($media);
            $media->set($fieldName, $form_state->getValue('json'));

            // Update media name after metadata are available.
            $source = $media->getSource();
            $media->setName($source->getMetadata($media, 'default_name'));
        }

        $form_state->setRebuild();
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     *
     * @return array
     */
    protected function buildActions(array $form, FormStateInterface $form_state)
    {
        $actions = parent::buildActions($form, $form_state);

        if (array_key_exists('save_select', $actions)) {
            $actions['save_select'] = [
                '#attributes' => array('class' => array('keepeek_save_insert')),
                '#type' => 'submit',
                '#button_type' => 'primary',
                '#value' => $this->t('Save and insert'),
                '#ajax' => [
                    'callback' => '::updateWidget',
                    'wrapper' => 'media-library-add-form-wrapper',
                ],
            ];
        }

        return $actions;
    }

}

