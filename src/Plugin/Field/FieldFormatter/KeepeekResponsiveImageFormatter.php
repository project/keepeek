<?php

namespace Drupal\keepeek\Plugin\Field\FieldFormatter;

use Drupal\keepeek\KeepeekFormatterInterface;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;

/**
 * Plugin implementation of the 'Keepeek Responsive Image' formatter.
 *
 * @FieldFormatter(
 *   id = "keepeek_responsive_image",
 *   label = @Translation("Keepeek Responsive Image"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class KeepeekResponsiveImageFormatter extends ResponsiveImageFormatter implements KeepeekFormatterInterface {

  use KeepeekImageFormatterTrait;

}
