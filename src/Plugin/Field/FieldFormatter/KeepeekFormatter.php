<?php

namespace Drupal\keepeek\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\keepeek\Service\KeepeekManager;

/**
 * Keepeek formatter.
 *
 * @FieldFormatter(
 *   id = "keepeek",
 *   label = @Translation("Keepeek"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class KeepeekFormatter extends KeepeekFormatterBase
{

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];
        foreach ($items as $delta => $item) {
            /**
             * @var FieldItemInterface $item 
             */
            if ($item->isEmpty()) {
                continue;
            }
            $data = KeepeekManager::getDataFromFieldItem($item);
            $isVideo = KeepeekManager::isVideo($data);
            $elements[$delta] = [
            '#theme' => 'keepeek',
            '#type' => "keepeek",
            '#url' => $isVideo ? KeepeekManager::getPermalinkUrl($data) : $item->value,
            '#is_video' => $isVideo,
            '#data' => $data,
            '#width' => $this->getSetting('width') ?? 960,
            '#height' => $this->getSetting('height') ?? 600,
            ];
        }
        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
        'width' => 960,
        'height' => 600,
        ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        return parent::settingsForm($form, $form_state) + [
        'width' => [
          '#type' => 'number',
          '#title' => $this->t('Width'),
          '#default_value' => $this->getSetting('width'),
          '#size' => 5,
          '#maxlength' => 5,
          '#field_suffix' => $this->t('pixels'),
          '#min' => 50,
        ],
        'height' => [
          '#type' => 'number',
          '#title' => $this->t('Height'),
          '#default_value' => $this->getSetting('height'),
          '#size' => 5,
          '#maxlength' => 5,
          '#field_suffix' => $this->t('pixels'),
          '#min' => 50,
        ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = parent::settingsSummary();
        $summary[] = $this->t(
            'Iframe size: %width x %height pixels', [
            '%width' => $this->getSetting('width'),
            '%height' => $this->getSetting('height'),
            ]
        );
        return $summary;
    }
}
