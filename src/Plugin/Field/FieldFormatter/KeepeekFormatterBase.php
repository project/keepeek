<?php

namespace Drupal\keepeek\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\media\Entity\MediaType;
use Drupal\keepeek\KeepeekFormatterInterface;
use Drupal\keepeek\Plugin\media\Source\KeepeekSource;
use Drupal\keepeek\Service\KeepeekManager;

/**
 * Base class for Keepeek formatter.
 */
abstract class KeepeekFormatterBase extends FormatterBase implements KeepeekFormatterInterface
{

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
        'formatter_class' => static::class,
        ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public static function isApplicable(FieldDefinitionInterface $field_definition)
    {
        // Only allow choosing this formatter if the media type is configured to
        // use the Keepeek source plugin.
        $entity_type_id = $field_definition->getTargetEntityTypeId();
        if ($entity_type_id === 'media') {
            $bundle = $field_definition->getTargetBundle();
            if (!empty($bundle)) {
                $media_type = MediaType::load($bundle);
                if (!empty($media_type)) {
                    $source = $media_type->getSource();
                    if ($source && ($source instanceof KeepeekSource)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public static function deriveMediaDefaultNameFromUrl($url)
    {
        $name = KeepeekManager::getFilenameFromUri($url);

        return strlen($name) > 255
        ? substr($name, 0, 252) . '...'
        : $name;
    }

}
