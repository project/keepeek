<?php

namespace Drupal\keepeek\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\keepeek\KeepeekFormatterInterface;

/**
 * Plugin implementation of the 'Keepeek Image' formatter.
 *
 * @FieldFormatter(
 *   id = "keepeek_image",
 *   label = @Translation("Keepeek Image"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class KeepeekImageFormatter extends ImageFormatter implements KeepeekFormatterInterface {

  use KeepeekImageFormatterTrait;

}
