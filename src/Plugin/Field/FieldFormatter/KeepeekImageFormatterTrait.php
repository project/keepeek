<?php

namespace Drupal\keepeek\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\keepeek\Plugin\Field\FieldFormatter\KeepeekFormatterBase;

/**
 * Provides a base implementation for a Keepeek formatter.
 */
trait KeepeekImageFormatterTrait {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'formatter_class' => static::class,
    ] + parent::defaultSettings();
  }

  /**
   * Returns the referenced entities for display.
   *
   * This method overrides the parent parameter type declaration, so we can
   * adapt the string field data type to temporary file references.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The item list.
   * @param string $langcode
   *   The language code of the referenced entities to display.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The array of referenced entities to display, keyed by delta.
   */
  protected function getEntitiesToView(FieldItemListInterface $items, $langcode) {
    // Mimic the field item list, so it can be processed by parent class.
    $file_items = EntityReferenceFieldItemList::createInstance($items->getDataDefinition(), $items->getName(), $items->getParent());
    $file_system = \Drupal::service('file_system');

    // Temporarily download every file to simulate file entity.
    foreach ($items as $index => $item) {
      $path = ltrim(parse_url($item->value, PHP_URL_PATH), '/.');
      $uri = 'public://keepeek/' . $path;
      if (!file_exists($uri)) {
        $directory = dirname($uri);
        if ($file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
          copy($item->value, $uri);
        }
      }
      // Fill simulated file entity data, so image styles are created.
      $item->entity = File::create(['uri' => $uri]);
      $item->_loaded = TRUE;
      $file_items->set($index, $item);
    }

    return parent::getEntitiesToView($file_items, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
    // Skip the parent logic to process everything inside getEntitiesToView().
  }

  /**
   * {@inheritdoc}
   */
  public static function deriveMediaDefaultNameFromUrl($url) {
    return KeepeekFormatterBase::deriveMediaDefaultNameFromUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return KeepeekFormatterBase::isApplicable($field_definition);
  }

}
