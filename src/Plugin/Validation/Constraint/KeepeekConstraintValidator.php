<?php

namespace Drupal\keepeek\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\keepeek\Plugin\media\Source\KeepeekSource;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates Keepeek URLs.
 */
class KeepeekConstraintValidator extends ConstraintValidator
{

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        /**
         * @var \Drupal\media\MediaInterface $media 
         */
        $media = $value->getEntity();
        $source = $media->getSource();
        if (!($source instanceof KeepeekSource)) {
            throw new \LogicException('Media source must implement ' . KeepeekSource::class);
        }

        $url = $source->getSourceFieldValue($media);
        $url = trim($url, "/");
        // The URL may be NULL if the source field is empty, which is invalid input.
        if (empty($url)) {
            $this->context->addViolation($constraint->emptyUrlMessage);
            return;
        }
    }

}
