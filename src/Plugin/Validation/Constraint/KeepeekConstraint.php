<?php

namespace Drupal\keepeek\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value represents a valid Keepeek URL.
 *
 * @Constraint(
 *   id = "keepeek",
 *   label = @Translation("Keepeek resource", context = "Validation"),
 *   type = {"string"}
 * )
 */
class KeepeekConstraint extends Constraint
{

    /**
     * The error message if the URL is empty.
     *
     * @var string
     */
    public $emptyUrlMessage = 'The URL cannot be empty.';

    /**
     * The error message if the URL does not match.
     *
     * @var string
     */
    public $invalidUrlMessage = 'The given URL is not valid. Valid values are in the format: @example_urls';

}
