<?php

namespace Drupal\keepeek\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\keepeek\Service\KeepeekManager;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Keepeek media source.
 *
 * @MediaSource(
 *   id = "keepeek",
 *   label = @Translation("Keepeek"),
 *   description = @Translation("Keepeek media"),
 *   allowed_field_types = {"string", "string_long"},
 *   default_thumbnail_filename = "generic.png",
 *   forms = {
 *     "media_library_add" = "Drupal\keepeek\Form\KeepeekMediaForm"
 *   }
 * )
 */

const KEEPEEK_MEDIA_TYPE = "keepeek_media_type";

class KeepeekSource extends MediaSourceBase implements MediaSourceFieldConstraintsInterface
{

    /**
     * Key for "Name" metadata attribute.
     *
     * @var string
     */
    const METADATA_ATTRIBUTE_NAME = 'name';

    const METADATA_ATTRIBUTE_JSON = 'json';

    /**
     * {@inheritdoc}
     */
    public function getMetadataAttributes()
    {
        return [
        static::METADATA_ATTRIBUTE_NAME => $this->t('Name'),
        static::METADATA_ATTRIBUTE_JSON => "JSON",
        ];
    }

    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

        $form = parent::buildConfigurationForm($form, $form_state);

        $form[KEEPEEK_MEDIA_TYPE] = [
            '#type' => 'checkboxes',
            '#title' => t('Choose the types of media that will be offered by the Keepicker'),
            '#default_value' => $this->configuration[KEEPEEK_MEDIA_TYPE] ?? ['PICTURE', 'VIDEO', 'DOCUMENT', 'SOUND', 'OTHER', 'RICH_MEDIA'],
            '#options' => [
                'PICTURE' => $this->t('Images'),
                'VIDEO' => $this->t('Videos'),
                'DOCUMENT' => $this->t('Documents'),
                'SOUND' => $this->t('Audios'),
                'OTHER' => $this->t('Other files'),
                'RICH_MEDIA' => $this->t('Rich media'),
            ],
            "#required" => true,
        ];

        return $form;
    }


    public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
        parent::submitConfigurationForm($form, $form_state);

        $mediaType = $form_state->getValue(KEEPEEK_MEDIA_TYPE);
        $this->configuration[KEEPEEK_MEDIA_TYPE] = $mediaType;
    }

    public function addJsonFieldToConfiguration($type)
    {
        $fieldName = parent::getSourceFieldName() . '_json';

        \Drupal::logger('keepeek')->info('Creating JSON field %fieldName', ['%fieldName' => $fieldName]);

        $this->entityTypeManager
            ->getStorage('field_storage_config')
            ->create(
                [
                'entity_type' => 'media',
                'field_name' => $fieldName,
                'type' => "string_long",
                ]
            )->save();


        \Drupal::logger('keepeek')->info('Creating JSON field configutation %fieldName inside bundle %bundle', ['%fieldName' => $fieldName, '%bundle' => $type->id()]);

        FieldConfig::create(
            [
            'bundle' => $type->id(),
            "entity_type" => "media",
            "field_name" => $fieldName,
            "label" => $fieldName,
            "description" => "",
            "required" => false,
            "default_value" => [],
            "default_value_callback" => "",
            "translatable" => false,
            "settings" => ["max_length" => 200000, "case_sensitive" => false, "is_ascii" => false],
            ]
        )->save();

        return $fieldName;
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadata(MediaInterface $media, $attribute_name)
    {
        $url = $media->get($this->configuration['source_field'])->value;
        // If the source field is not required, it may be empty.
        if (empty($url)) {
            return parent::getMetadata($media, $attribute_name);
        }
        switch ($attribute_name) {
        case static::METADATA_ATTRIBUTE_NAME:
        case 'default_name':
            // Use asset title as the default name if available.
            $data = KeepeekManager::getDataFromMedia($media);
            if (!empty($data['title']['value'])) {
                return strlen($data['title']['value']) > 255
                    ? substr($data['title']['value'], 0, 252) . '...'
                    : $data['title']['value'];
            }
            // Formatters know how to convert the URL into a default name string.
            $formatter_class = $this->getFormatterClass($media);
            return $formatter_class::deriveMediaDefaultNameFromUrl($url);

        case 'thumbnail_uri':
            return $url;

        default:
            return parent::getMetadata($media, $attribute_name);
        }
    }

    /**
     * Figure out the formatter class to be used on a given media entity.
     *
     * @param \Drupal\media\MediaInterface $media
     *   The media entity we are interested in.
     *
     * @return string
     *   The FQN of the formatter class configured in the `default` media display
     *   for the media source field.
     */
    public function getFormatterClass(MediaInterface $media)
    {
        $field_definition = $this->getSourceFieldDefinition($media->bundle->entity);

        // @todo There is probably a better way for this class to figure out what
        // formatter class is being used.
        /**
         * @var EntityViewDisplayInterface $display 
         */
        $display = EntityViewDisplay::load('media.' . $media->bundle() . '.default');
        $components = $display->getComponents();
        $formatter_config = $components[$field_definition->getName()] ?? [];
        if (empty($formatter_config['settings']['formatter_class'])) {
            throw new \LogicException('The Keepeek validator needs the _default_ media display to be configured, and for the source field to use any of the formatters provided by the Keepeek module.');
        }

        // See KeepeekFormatterBase::defaultSettings() for where this is
        // defined/enforced.
        return $formatter_config['settings']['formatter_class'];
    }

    /**
     * {@inheritdoc}
     */
    public function getSourceFieldConstraints()
    {
        return [
        'keepeek' => [],
        ];
    }
}
