<?php

namespace Drupal\keepeek\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\media\Entity\Media;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides route responses for the Keepeek module.
 */
class KeepeekEndpoint extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(Connection $connection, LoggerInterface $logger) {
    $this->connection = $connection;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('logger.factory')->get('keepeek')
    );
  }

  /**
   * Purge all generated media image styles for specified assets.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function purge(Request $request) {
    $purged = [];
    foreach ($this->getMediaIds($request) as $media_id) {
      $entity = Media::load($media_id);
      if (!empty($entity)) {
        _keepeek_flush_image_styles($entity);
        $purged[] = $media_id;
      }
    }

    // Log debug information with request result.
    $this->logger->info('KeepeekEndpoint purged: %data', [
      '%data' => print_r($purged, TRUE),
    ]);

    if (!empty($purged)) {
      return new JsonResponse(['message' => 'Assets purged successfully.']);
    }
    else {
      return new JsonResponse(['message' => 'No assets purged.']);
    }
  }

  /**
   * Unpublish all media entities related to the specified assets.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function delete(Request $request) {
    $deleted = [];
    foreach ($this->getMediaIds($request) as $media_id) {
      $entity = Media::load($media_id);
      if (!empty($entity)) {
        if ($entity->isPublished()) {
          $entity->setUnpublished();
          $entity->save();
          $deleted[] = $media_id;
        }
        _keepeek_flush_image_styles($entity);
      }
    }

    // Log debug information with request result.
    $this->logger->info('KeepeekEndpoint deleted: %data', [
      '%data' => print_r($deleted, TRUE),
    ]);

    if (!empty($deleted)) {
      return new JsonResponse(['message' => 'Assets deleted successfully.']);
    }
    else {
      return new JsonResponse(['message' => 'No assets deleted.']);
    }
  }

  /**
   * Get media IDs from the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   *   The media IDs.
   */
  protected function getMediaIds(Request $request) {
    // Validate JWT key is configured and authorization header exists.
    $jwt_key = $this->config('keepeek.settings')->get('jwt_key');
    if (empty($jwt_key)) {
      throw new BadRequestHttpException('The endpoint JWT key is not configured.');
    }
    $authorization = $request->headers->get('Authorization');
    if (empty($authorization)) {
      throw new BadRequestHttpException('The request JWT token is missing.');
    }

    // Parse data from JWT token inside request authorization header.
    try {
      $jwt_token = str_replace('Bearer ', '', $authorization);
      $data = JWT::decode($jwt_token, new Key($jwt_key, 'HS256'));
    }
    catch (\Exception $e) {
      throw new BadRequestHttpException('The JWT validation problem: ' . $e->getMessage());
    }

    // Log debug information with request details.
    $this->logger->debug('KeepeekEndpoint request: %data', [
      '%data' => print_r([
        'path' => $request->getPathInfo(),
        'token' => $authorization,
        'data' => $data,
      ], TRUE),
    ]);

    // Check if asset IDs are provided.
    if (empty($data->asset_ids)) {
      throw new BadRequestHttpException('The asset IDs are not provided.');
    }
    // Allow only a certain number of assets to be processed at once.
    elseif (count($data->asset_ids) > 100) {
      throw new BadRequestHttpException('Too many asset IDs were provided.');
    }

    // Build database query to select all related entities.
    $query = $this->connection->select('media__field_media_keepeek_1_json', 'f');
    $query->addField('f', 'entity_id');
    $or_group = $query->orConditionGroup();
    foreach ($data->asset_ids as $asset_id) {
      $or_group->condition('f.field_media_keepeek_1_json_value', '{"id":' . $asset_id . ',%', 'LIKE');
    }
    $query->condition($or_group);
    return $query->execute()->fetchCol();
  }

}
