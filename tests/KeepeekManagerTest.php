<?php

namespace Drupal\keepeek\Tests;

use Drupal\keepeek\Service\KeepeekManager;
use PHPUnit\Framework\TestCase;

class KeepeekManagerTest extends TestCase
{
    public function testGetCorrectUrlWithInnerUrlAndItokParam()
    {
        $url = 'https://google.com/https/keepeek.com/path/to/picture.png?itok=12345';
        $expected = 'https://keepeek.com/path/to/picture.png';
        $this->assertEquals($expected, KeepeekManager::getCorrectUrl($url));
    }

    public function testGetCorrectUrlWithInnerUrl()
    {
        $url = 'https://google.com/https/keepeek.com/path/to/picture.png';
        $expected = 'https://keepeek.com/path/to/picture.png';
        $this->assertEquals($expected, KeepeekManager::getCorrectUrl($url));
    }

    public function testGetCorrectUrlWithoutInnerUrl()
    {
        $url = 'https://keepeek.com/path/to/picture.png';
        $expected = 'https://keepeek.com/path/to/picture.png';
        $this->assertEquals($expected, KeepeekManager::getCorrectUrl($url));
    }

    public function testTypeNormal()
    {
        $uri = 'https://keepeek.com/path/to/picture.png';
        $expected = 'type-normal';
        $this->assertEquals($expected, KeepeekManager::getUriType($uri));
    }

    public function testTypeCrop()
    {
        $uri = 'https://keepeek.com/path/to/crop/picture.png';
        $expected = 'type-resize';
        $this->assertEquals($expected, KeepeekManager::getUriType($uri));
    }

    public function testTypeResize()
    {
        $uri = 'https://keepeek.com/path/to/resize/picture.png';
        $expected = 'type-resize';
        $this->assertEquals($expected, KeepeekManager::getUriType($uri));
    }

    public function testGetFilenameFromResizeUri()
    {
        $uri = 'https://keepeek.com/path/to/resize/picture.png';
        $expected = 'picture.png';
        $this->assertEquals($expected, KeepeekManager::getFilenameFromUri($uri));
    }
    public function testGetFilenameFromUri()
    {
        $uri = 'https://keepeek.com/path/to/picture.png';
        $expected = 'picture.png';
        $this->assertEquals($expected, KeepeekManager::getFilenameFromUri($uri));
    }

    public function testGetPermalinkUrlWithMediumType()
    {
        $data = json_decode('{"permalinks": [{"title": "preview", "url": "https://keepeek.com/preview.jpg"},{"title": "medium", "url": "https://keepeek.com/medium.jpg"}]}', true);
        $expected = 'https://keepeek.com/medium.jpg';
        $this->assertEquals($expected, KeepeekManager::getPermalinkUrl($data, KeepeekManager::PERMALINK_TYPE_MEDIUM));
    }

    public function testGetPermalinkUrlWithPreviewType()
    {
        $data = json_decode('{"permalinks": [{"title": "preview", "url": "https://keepeek.com/preview.jpg"},{"title": "medium", "url": "https://keepeek.com/medium.jpg"}]}', true);
        $expected = 'https://keepeek.com/preview.jpg';
        $this->assertEquals($expected, KeepeekManager::getPermalinkUrl($data, KeepeekManager::PERMALINK_TYPE_PREVIEW));
    }

    public function testGetPermalinkUrlWithXlargeType()
    {
        $data = json_decode('{"permalinks": [{"title": "preview", "url": "https://keepeek.com/preview.jpg"},{"title": "medium", "url": "https://keepeek.com/medium.jpg"}]}', true);
        $expected = '';
        $this->assertEquals($expected, KeepeekManager::getPermalinkUrl($data, KeepeekManager::PERMALINK_TYPE_XLARGE));
    }

    public function testIsVideoWithVideoUrl()
    {
        $data = json_decode('{"mediaType": "video/mp4"}', true);
        $this->assertTrue(KeepeekManager::isVideo($data));
    }

    public function testIsVideoWithImageUrl()
    {
        $data = json_decode('{"mediaType": "image/png"}', true);
        $this->assertFalse(KeepeekManager::isVideo($data));
    }
}