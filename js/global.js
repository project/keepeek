/**
 * we wait for the end of the loading of the article edition popup before injecting the webcomponent in order to avoid problems with mounting/unmounting the webcomponent (and random behavior on the react side).
 * if you need to inject information from the drupal backend, you can go through "drupalSettings" https://www.drupal.org/docs/develop/creating-modules/adding-assets-css-js-to-a-drupal-module-via-librariesyml#configurable
 * @type {{attach: Drupal.behaviors.addKeepickerWebComponentMarkup.attach}}
 */
(
  function (Drupal, drupalSettings) {
    Drupal.behaviors.addKeepickerWebComponentMarkup = {
      attach: function (context) {
        var keepickerContainer = document.getElementById(
          "kpk-keepicker-container"
        );
        if (
          (context.id === "node-article-edit-form" ||
            context.id === "media-library-content" ||
            context.id === "media-library-wrapper") &&
          keepickerContainer !== null
        ) {
          // we have to change the z-index of the "media-library-content" container
          // because of a drupal bug that makes this container behind the buttons:
          // "insert selected item" and the media source selection buttons when there are several
          // => This z index will be cleared as soon as the media source changes
          var mediaLibraryContent = document.getElementById(
            "media-library-content"
          );
          if (mediaLibraryContent) {
            mediaLibraryContent.style.zIndex = "2";
          }

          // For some reason Drupal attach and detach form while opening media library popup
          // Use a setTimeout to avoid adding and removing web component from the DOM because
          // React generates a 409 error (Cannot update an unmounted root)
          setTimeout(() => {
            var keepicker = document.createElement("kpk-keepicker");
            keepicker.setAttribute("id", "keepicker");

            var apiEndpoint = drupalSettings.keepeek.api_endpoint;
            var keycloakClientId = drupalSettings.keepeek.keycloak_client_id;
            var keycloakIdp = drupalSettings.keepeek.keycloak_idp;
            var dataLocale = drupalSettings.keepeek.data_locale;
            var uiLocale = drupalSettings.keepeek.ui_locale;
            var filterOnKeepeekMediaTypeArray = convertToMediaTypeArray(
              keepickerContainer.getAttribute("data-keepeek-media-type")
            );

            if (apiEndpoint) {
              keepicker.setAttribute("api-endpoint", apiEndpoint);
            }
            if (keycloakClientId) {
              keepicker.setAttribute("keycloak-client-id", keycloakClientId);
            }
            if (keycloakIdp) {
              keepicker.setAttribute("keycloak-idp", keycloakIdp);
            }
            if (dataLocale) {
              keepicker.setAttribute("data-locale", dataLocale);
            }
            if (uiLocale) {
              keepicker.setAttribute("ui-locale", uiLocale);
            }
            if (filterOnKeepeekMediaTypeArray.length > 0) {
              keepicker.setAttribute(
                "custom-global-configuration",
                getCustomGlobalConfigurationForMediaType(
                  filterOnKeepeekMediaTypeArray
                )
              );
            }

            keepicker.addEventListener("kpk-insert", async (event) => {
              setKeepickerFormValues(
                getUrlFromDetail(event.detail),
                JSON.stringify(event.detail.element)
              );
            });
            keepicker.addEventListener("kpk-insert-link", async (event) => {
              setKeepickerFormValues(
                getUrlFromDetail(event.detail),
                JSON.stringify(event.detail.element)
              );
            });
            keepickerContainer.innerHTML = "";
            keepickerContainer.appendChild(keepicker);

            var drupalModal = document.getElementById("drupal-modal");
            if (drupalModal) {
              //To scroll at top when user is not connected to Keepicker so to see authentication box. See RF-2811
              drupalModal.scrollTop = 0;
            }
          }, 1);
        }
      },
    };
  }
)(Drupal, drupalSettings);

/**
 * Drupal behavior to resize the media library dialog.
 * This behavior is attached to the window object and listens for the "dialog:beforecreate" event.
 * When this event is triggered, it checks if the dialog being created is the media library widget modal.
 * If it is, it sets the height and width of the dialog to 95% of the window's height and width respectively.
 * It also sets up a resize function to adjust the dialog's height when the window is resized.
 * This resize function is removed when the dialog is closed.
 *
 * @param {Object} Drupal - The Drupal object.
 */
(function ($, Drupal) {
  Drupal.behaviors.resizeMediaLibraryDialog = {
    attach: function () {
      // Listen for the "dialog:beforecreate" event on the window object.
      $(window).on(
        "dialog:beforecreate",
        function (event, dialog, $element, settings) {
          // Check if the dialog being created is the media library widget modal.
          if (isMediaLibraryPopup(settings)) {
            // Set the height and width of the dialog to 95% of the window's height and width respectively.
            settings.height = $(window).height() * 0.95; // can't apply height in %, due to jquery-ui dialog limitation
            settings.width = "95%";
            // remove "ui-dialog--narrow" class (for drupal 11):
            if (settings.classes && settings.classes["ui-dialog"]) {
              settings.classes["ui-dialog"] = settings.classes[
                "ui-dialog"
              ].replaceAll("ui-dialog--narrow", "");
            }
            // Define a resize function to adjust the dialog's height when the window is resized.
            var resizeFunction = function () {
              if ($element) {
                $element.dialog("option", "height", $(window).height() * 0.95);
              }
            };

            // Attach the resize function to the window's "resize" event.
            $(window).on("resize", resizeFunction);

            // Remove the resize function when the dialog is closed.
            $element.on("dialogclose", function () {
              $(window).off("resize", resizeFunction);
            });
          }
        }
      );
    },
  };
})(jQuery, Drupal);


/**
 * Checks if the given settings correspond to a media library popup.
 *
 * @param {Object} settings - The settings object to check.
 * @param {Array} settings.buttons - An array of button objects in the settings.
 * @param {string|String} settings.buttons[].class - The class of the first button.
 * @returns {boolean} - True if the settings correspond to a media library popup, false otherwise.
 */
function isMediaLibraryPopup(settings) {
  return (
    settings &&
    settings.buttons &&
    Array.isArray(settings.buttons) &&
    settings.buttons.length > 0 &&
    settings.buttons[0].class &&
    (typeof settings.buttons[0].class === "string" ||
      settings.buttons[0].class instanceof String) &&
    settings.buttons[0].class.includes("media-library")
  );
}

/**
 * Extracts a URL from the provided detail object.
 *
 * This function attempts to retrieve a URL from the given detail object by checking various properties.
 * It first checks if the detail object has a 'link' property and returns it if present.
 * If not, it looks for a 'permalinks' array within the 'element' property of the detail object and searches for URLs with specific titles ('whr' or 'preview').
 * If no URLs are found in the permalinks, it checks for a 'previewUrl' property within the 'element' property.
 * If none of these conditions are met, it returns undefined.
 *
 * @param {Object} detail - The detail object containing potential URL information.
 * @param {string} [detail.link] - An optional direct link.
 * @param {Object} [detail.element] - An optional element object containing permalinks or a preview URL.
 * @param {Array} [detail.element.permalinks] - An optional array of permalinks within the element object.
 * @param {string} [detail.element.previewUrl] - An optional preview URL within the element object.
 * @returns {string|undefined} - The extracted URL or undefined if no URL is found.
 */
function getUrlFromDetail(detail) {
  if (!detail) {
    return undefined;
  }
  if ("link" in detail && detail.link) {
    return detail.link;
  }
  if (detail.element) {
    if (detail.element.permalinks) {
      var permalinks = detail.element.permalinks;
      var whrUrl = permalinks.find(
        (permalink) => permalink.title === "whr"
      )?.url;
      if (whrUrl) {
        return whrUrl;
      }
      var previewUrl = permalinks.find(
        (permalink) => permalink.title === "preview"
      )?.url;
      if (previewUrl) {
        return previewUrl;
      }
    }
    if (detail.element.previewUrl) {
      return detail.element.previewUrl;
    }
  }
  return undefined;
}

function setKeepickerFormValues(url, json) {
  document.querySelector(".keepicker-url").value = url;
  document.querySelector(".keepicker-json").value = json;
  submitForm();
}

function submitForm() {
  document
    .querySelector("#media-library-add-form-wrapper input.form-submit")
    .click();
}

/**
 * Returns a custom global configuration object for the given media type array.
 * @param mediaTypeArray
 * @return {undefined|string}
 */
function getCustomGlobalConfigurationForMediaType(mediaTypeArray) {
  if (Array.isArray(mediaTypeArray)) {
    return JSON.stringify({
      permanentFilters: [
        {
          values: mediaTypeArray,
          type: "LISTFIELD",
          internalFieldName: "metaModelType",
          fieldType: "MEDIAFIELD",
          modifier: "EQUALS_ONE",
        },
      ],
    });
  } else {
    return undefined;
  }
}

function convertToMediaTypeArray(mediaTypeAttribute) {
  var ret = [];
  var mediaTypeAttributeJSON = JSON.parse(mediaTypeAttribute);
  // if JSON contain key "audios" and value is not 0 : enable audios :
  if (mediaTypeAttributeJSON.PICTURE !== 0) {
    ret.push("1");
  }
  if (mediaTypeAttributeJSON.DOCUMENT !== 0) {
    ret.push("2");
  }
  if (mediaTypeAttributeJSON.VIDEO !== 0) {
    ret.push("3");
  }
  if (mediaTypeAttributeJSON.OTHER !== 0) {
    ret.push("4");
  }
  if (mediaTypeAttributeJSON.SOUND !== 0) {
    ret.push("5");
  }
  if (mediaTypeAttributeJSON.RICH_MEDIA !== 0) {
    ret.push("6");
  }
  return ret;
}
