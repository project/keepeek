# Keepeek Drupal Plugin

## Overview

This plugin is used to connect your Drupal site to the Keepeek DAM platform.

## Configuration & Use

Contact the Keepeek team to enable the Drupal module.

1. Install the Keepeek module in `Extensions > List`.
2. Configure the Keepeek plugin in `Configuration > Keepeek`.
3. Create a new Keepeek media type in `Structure > Media Type`:
    - Click on `Add new media type`.
    - Fill in the name.
    - Select Keepeek as the media source.
    - Source field: select an existing field or, by default, a new one will be created automatically.
    - JSON field: select an existing field or, by default, a new one will be created automatically.
    - Click on "Create".
    - In the `Manage Display` tab:
      - For the Keepeek field, select the Keepeek format.
      - Make sure the JSON field is not displayed.
      - Save the field display.
4. Associate the created media type with a content type in `Structure > Content type`:
    - Click on `Manage Fields` for a content type.
    - Click on `Create New Field`.
    - In the combo box, select the `Media` field and fill in the label.
    - Click on `Add`.
    - Click on `Save Field Settings`.
    - In `Keepeek Field Settings`, select the Keepeek media type.
5. The Keepeek module is now ready.

## Credit

This module have been developed on Media Remote module code base. 